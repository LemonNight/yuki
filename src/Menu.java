import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Created by patri on 31.03.2017.
 */
public class Menu extends SpielObjekt {

    private Image menu = null;

    public Menu(int mapX, int mapY, Image image) throws SlickException {
        super(mapX, mapY, image);
        menu = image;
    }

    public void draw(Graphics g) {
        image.drawCentered(mapX * 32, mapY * 32);
    }


    @Override
    public void update(int delta) {
    }
}
