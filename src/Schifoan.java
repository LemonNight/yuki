import org.newdawn.slick.*;
import org.newdawn.slick.tiled.TiledMap;

/**
 * Created by x13So on 27.01.2017.
 */
public class Schifoan extends BasicGame {

    private TiledMap map;
    private TiledMap mapLevel1;
    private TiledMap mapLevel2;
    private TiledMap mapLevel3;
    private Skifahrer skifahrer;
    private Menu menu;
    private Level level;
    private Help help;
    private int updateCounter = 0;
    private int grenzLayer = 0;
    private int hindernissLayer = 0;
    private int geschwindigkeit = 5;
    private int versuche = 1;
    private int stufe = 1;
    private int gamestat = 0;
    Font scoreFont;
    Font gameFont;


    public Schifoan() {
        super("Schifoan");
    }

    public static void main(String[] args) throws SlickException {
        AppGameContainer container = new AppGameContainer(new Schifoan());
        container.setDisplayMode(960, 1080, false);
        container.setClearEachFrame(false);
        container.setMinimumLogicUpdateInterval(25);
        container.start();
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        if (gamestat == 0) {
            menu.draw(g);
        }

        if (gamestat == 1) {
            level.draw(g);
        }

        if (gamestat == 2) {
            map.render(0, 0);
            skifahrer.draw(g);
            scoreFont.drawString(10, 100, " " + versuche);
            scoreFont.drawString(865, 100, " " + stufe);
        }

        if (gamestat == 3){
            help.draw(g);
        }


    }

    @Override
    public void init(GameContainer container) throws SlickException {
        mapLevel1 = new TiledMap("res/tiles/Yuki_blau.tmx");
        mapLevel2 = new TiledMap("res/tiles/Yuki_rot.tmx");
        mapLevel3 = new TiledMap("res/tiles/Yuki_schwarz.tmx");
        map = mapLevel1;
        grenzLayer = map.getLayerIndex("Grenze");
        hindernissLayer = map.getLayerIndex("Hindernisse");

        skifahrer = new Skifahrer(15, 1, new Image("res/Punkt-4.png"));

        menu = new Menu(15, 17, new Image("res/Menu.png"));

        level = new Level(15, 17, new Image("res/Level.png"));

        help = new Help(15, 17, new Image("res/Help.png"));

        Music music = new Music("res/sounds/background.ogg");
        music.loop();
        scoreFont = new AngelCodeFont("res/fonts/score_numer_font.fnt", new Image("res/fonts/score_numer_font.png"));
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        if (gamestat == 0) {

            if (container.getInput().isKeyPressed(Input.KEY_UP)) {
                gamestat = 1;
            } else if (container.getInput().isKeyPressed(Input.KEY_F1)) {
                gamestat = 3;
            } else if (container.getInput().isKeyPressed(Input.KEY_DOWN)) {
                container.exit();
            } else if (container.getInput().isKeyPressed(Input.KEY_ESCAPE)) {
                container.exit();
            }

        }
        if (gamestat == 1) {

            level = new Level(15, 17, new Image("res/Level.png"));

            if (container.getInput().isKeyPressed(Input.KEY_ESCAPE)) {
                gamestat = 0;

            } else if (container.getInput().isKeyPressed(Input.KEY_1)) {
                map = mapLevel1;
                grenzLayer = map.getLayerIndex("Grenze");
                hindernissLayer = map.getLayerIndex("Hindernisse");
                gamestat = 2;
            } else if (container.getInput().isKeyPressed(Input.KEY_2)) {
                map = mapLevel2;
                grenzLayer = map.getLayerIndex("Grenze");
                hindernissLayer = map.getLayerIndex("Hindernisse");
                gamestat = 2;

            } else if (container.getInput().isKeyPressed(Input.KEY_3)) {
                map = mapLevel3;
                grenzLayer = map.getLayerIndex("Grenze");
                hindernissLayer = map.getLayerIndex("Hindernisse");
                gamestat = 2;
            }
            skifahrer = new Skifahrer(15, 1, new Image("res/Punkt-4.png"));
            Music music = new Music("res/sounds/background.ogg");
            music.loop();
            scoreFont = new AngelCodeFont("res/fonts/score_numer_font.fnt", new Image("res/fonts/score_numer_font.png"));

        }
        if (gamestat == 2) {
            updateCounter++;
            if (skifahrer.getMapY() >= 30) {
                skifahrer.setMapY(1);
                skifahrer.setMapX(15);
                geschwindigkeit--;
                versuche = 1;
                System.out.println(versuche);
                stufe++;

            }

            if (container.getInput().isKeyPressed(Input.KEY_ESCAPE)) {
                gamestat = 0;
            }

            if (updateCounter > geschwindigkeit
                    && (map.getTileId(skifahrer.getMapX(), skifahrer.getMapY() + 1, hindernissLayer) == 0)
                    && (map.getTileId(skifahrer.getMapX() - 1, skifahrer.getMapY() + 1, hindernissLayer) == 0)) {
                skifahrer.setMapY(skifahrer.getMapY() + 1);
                updateCounter = 0;
            }

            if (container.getInput().isKeyPressed(Input.KEY_RIGHT)) {
                if ((map.getTileId(skifahrer.getMapX() + 1, skifahrer.getMapY(), grenzLayer) == 0) && (map.getTileId(skifahrer.getMapX() + 1, skifahrer.getMapY(), hindernissLayer) == 0)) {
                    skifahrer.setMapX(skifahrer.getMapX() + 1);
                    skifahrer.setRightOriented(true);
                }
            }

            if (container.getInput().isKeyPressed(Input.KEY_LEFT)) {
                if ((map.getTileId(skifahrer.getMapX() - 1, skifahrer.getMapY(), grenzLayer) == 0) && (map.getTileId(skifahrer.getMapX() - 2, skifahrer.getMapY(), hindernissLayer) == 0)) {
                    skifahrer.setMapX(skifahrer.getMapX() - 1);
                    skifahrer.setRightOriented(false);

                }
            }
            if ((map.getTileId(skifahrer.getMapX() - 1, skifahrer.getMapY() + 1, hindernissLayer) > 0)) {
                skifahrer.setMapY(1);
                skifahrer.setMapX(15);
                versuche++;
                geschwindigkeit++;
            }
            if ((map.getTileId(skifahrer.getMapX(), skifahrer.getMapY() + 1, hindernissLayer) > 0)) {
                skifahrer.setMapY(1);
                skifahrer.setMapX(15);
                versuche++;
                geschwindigkeit++;
            }

        }

        if (gamestat == 3) {
            help = new Help(15, 17, new Image("res/Help.png"));
            if (container.getInput().isKeyPressed(Input.KEY_ESCAPE)) {
                gamestat = 0;
            }

        }

    }
}

