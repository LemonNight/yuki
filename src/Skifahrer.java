import org.newdawn.slick.*;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 * Created by x13So on 03.02.2017.
 */


public class Skifahrer extends SpielObjekt {

    private Image skifahrerLeft = null;
    private Image skifahrerRight = null;
    private boolean isRightOriented = true;

    public Skifahrer(int mapX, int mapY, Image image) throws SlickException {
        super(mapX, mapY, image);
        skifahrerLeft = image.getFlippedCopy(true, false);
        skifahrerRight = image;
    }

    public void draw(Graphics g) {
        if (isRightOriented) {
            image = skifahrerRight;
        } else {
            image = skifahrerLeft;
        }


        image.drawCentered(mapX * 32, mapY * 32);
        image.drawCentered(mapX * 32, mapY * 32);

    }

    @Override
    public void update(int delta) {
    }

    public void setRightOriented(boolean isRightOriented) {
        this.isRightOriented = isRightOriented;
    }
}