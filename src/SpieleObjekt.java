import org.newdawn.slick.*;

/**
 * Abstrakte Basisklasse für ein Spielobjekt (kann nicht instanziert werden).
 */
abstract class SpielObjekt {
    protected int mapX;
    protected int mapY;
    protected Image image;

    public int getMapX() {
        return mapX;
    }

    public void setMapX(int mapX) {
        this.mapX = mapX;
    }

    /**
     * Zeichnet das Spielobjekt - jede abgeleitete Klasse muss diese Methode implementieren (Abstrakte Definition -
     * --> Polymorphismus)
     *
     * @param g Zugriff auf die Graphik des Spiels
     */
    public abstract void draw(Graphics g);

    /**
     * Wird bei jedem Spieldurchgang (Spiel-loop) aufgerufen.
     * Jede abgeleitete Klasse muss diese Methode implementieren (Abstrakte Definition -
     * --> Polymorphismus)
     *
     * @param delta Zeitdifferenz zum letzten Aufruf
     */
    public void update(int delta) {
    }

    ;

    /**
     * Konstruktor mit Übergabewerten
     *
     * @param x     x-Koordinate
     * @param y     y-Koordinate
     * @param image Graphik der Spielfigur
     */
    public SpielObjekt(int mapX, int mapY, Image image) {
        this(mapX, mapY);
        this.image = image;
    }

    /**
     * Konstruktor mit Übergabewerten
     *
     * @param x x-Koordinate
     * @param y y-Koordinate
     */
    public SpielObjekt(int mapX, int mapY) {
        this.mapX = mapX;
        this.mapY = mapY;
    }

    /**
     * Konstruktor mit Übergabewerten
     *
     * @param image Graphik der Spielfigur
     */
    public SpielObjekt(Image image) {
        this.image = image;
    }

    /**
     * Default-Konstruktor ohne Uebergabewerte
     */
    public SpielObjekt() {
    }

    public int getMapY() {
        return mapY;
    }

    public void setMapY(int mapY) {
        this.mapY = mapY;
    }

}