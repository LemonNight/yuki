import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Created by x13So on 31.03.2017.
 */
public class Help extends SpielObjekt {
    private Image help = null;

    public Help(int mapX, int mapY, Image image) throws SlickException {
        super(mapX, mapY, image);
        help = image;
    }

    public void draw(Graphics g) {
        image.drawCentered(mapX * 32, mapY * 32);
    }

    @Override
    public void update(int delta) {
    }

}
